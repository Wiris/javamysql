
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Persistencia {

    public void ExecutarSQL(String sql, Conexao con) {
        try {
            Statement requisicao = con.getCon().createStatement();
            requisicao.execute(sql);
        } catch (SQLException ex) {
            System.out.println("Ocorreu um erro ao executar o SQL");
        }
    }

    public ResultSet ConsultaSQL(String sql, Conexao con) {
        ResultSet resultado = null;
        try {
            Statement consulta = con.getCon().createStatement();
            resultado = consulta.executeQuery(sql);
        } catch (SQLException ex) {
            System.out.println("Ocorreu um erro ao criar o Statement");
            return null;
        }
        return resultado;
    }
}
