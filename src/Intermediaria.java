
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Intermediaria {
    
    private Persistencia persist;
    private Conexao conector;
    
    public Intermediaria() {
        this.persist = new Persistencia();
        this.conector = new Conexao();
    }
    
    public List<Aluno> consultaAlunos() {
        ResultSet dados;
        String sql = "SELECT * FROM academico2022.aluno;";
        dados = this.persist.ConsultaSQL(sql, this.conector);
        List<Aluno> listAlunos = new ArrayList<>();
        if (!Objects.isNull(dados)) {
            try {
                while (dados.next()) {
                    Aluno aluno = new Aluno(dados.getInt("Codigo"), dados.getString("Nome"));
                    listAlunos.add(aluno);
                }
            } catch (SQLException ex) {
                System.out.println("Ocorreu um erros ao ler os dados!");
            }
            return listAlunos;
        }
        return null;
    }
    
    public void inserirAluno(Aluno aluno) {
        String sql = "INSERT INTO academico2022.aluno (nome) VALUES('" + aluno.getNome() + "');";
        this.persist.ExecutarSQL(sql, conector);
    }
    
    public Aluno atualizarAluno(Aluno aluno) {
        String sql = "UPDATE academico2022.aluno SET nome = '" + aluno.getNome() + "' WHERE codigo = " + aluno.getCodigo() + ";";
        this.persist.ExecutarSQL(sql, conector);
        return getById(aluno.getCodigo());
    }
    
    public Aluno getById(int id) {
        String sql = "SELECT * FROM academico2022.aluno WHERE codigo = " + id + ";";
        ResultSet dados = this.persist.ConsultaSQL(sql, conector);
        try {
            while (dados.next()) {
                Aluno aluno = new Aluno(dados.getInt("Codigo"), dados.getString("Nome"));;
                return aluno;
            }
        } catch (SQLException ex) {
            Logger.getLogger(Intermediaria.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
}
