
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Conexao {

    private Connection con;

    Conexao(String url, String usuario, String senha) {
        this.EstabeleceConexao(url, usuario, senha);
    }

    Conexao() {
        String url = "jdbc:mysql://localhost:3306/academico2022?" + "useTimeZone=true&serverTimeZone=UTC&autoReconnect=true&useSSL=false";
        String usuario = "root";
        String senha = "root";

        this.EstabeleceConexao(url, usuario, senha);
    }

    public Connection getCon() {
        return this.con;
    }

    /**
     * Carrega o driver de conexao e realiza a conexao
     */
    private void EstabeleceConexao(String url, String usuario, String senha) {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            System.out.println("Erro ao carregar o driver");
        }
        
        try {
            this.con = DriverManager.getConnection(url, usuario, senha);
        } catch (SQLException e) {
            System.out.println("Erro ao realizar a conex�o");
        }
        
        if (this.con == null) {
            System.out.println("Conex�o com o BD n�o estabelecida");
        } else {
            System.out.println("Conex�o estabelecida");
        }

        
    }
}
