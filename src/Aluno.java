public class Aluno {
    private int Codigo;
    private String Nome;
    
    public Aluno(int Codigo, String Nome){
        this.Codigo = Codigo;
        this.Nome = Nome;
    }
    
    public Aluno(String Nome){
        this.Nome = Nome;
    }

    public void setCodigo(int Codigo) {
        this.Codigo = Codigo;
    }

    public void setNome(String Nome) {
        this.Nome = Nome;
    }

    public int getCodigo() {
        return Codigo;
    }
    
    public String getNome() {
        return Nome;
    }
    
}
