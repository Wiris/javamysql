
import java.util.List;
import java.util.Scanner;

public class Principal {

    public static void main(String[] args) {
//        inserir();
//        buscar();
        atualizar();

    }

    public static void inserir() {
        Scanner leitor = new Scanner(System.in);
        Intermediaria intermed = new Intermediaria();

        System.out.println("Dgite o nome do Aluno: ");
        String nome = leitor.nextLine();
        Aluno novoAluno = new Aluno(nome);
        intermed.inserirAluno(novoAluno);
    }

    public static void buscar() {
        Intermediaria intermed = new Intermediaria();
        List<Aluno> listAlunos = intermed.consultaAlunos();
        System.out.println("----------Alunos----------");
        listAlunos.forEach((aluno) -> {
            System.out.println("Nome: " + aluno.getNome());
        });
    }

    public static void atualizar() {
        Scanner leitor = new Scanner(System.in);
        Intermediaria intermed = new Intermediaria();

        List<Aluno> listAlunos = intermed.consultaAlunos();
        System.out.println("----------Alunos----------");
        for (int count = 0; count < listAlunos.size(); count++) {
            System.out.println(count + 1 + " - Nome: " + listAlunos.get(count).getNome());
        }

        System.out.println("Escolha um aluno para alterar: ");
        int codigo = leitor.nextInt();

        codigo = codigo-1;
        leitor.nextLine();

        System.out.println("Digite o novo nome do Aluno: ");
        String nome = leitor.nextLine();

        listAlunos.get(codigo).setNome(nome);
        Aluno aluno;
        aluno = intermed.atualizarAluno(listAlunos.get(codigo));
        System.out.println("Nome: " + aluno.getNome());
    }

}
